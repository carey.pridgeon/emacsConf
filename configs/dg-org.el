;; Configuration for Org Mode
(provide 'dg-org)

;;Load relevant modules here

(require 'ox-beamer)
(require 'ox-latex)
(require 'ox-reveal)

;;Babel highlights
(setq org-src-fontify-natively t)

;;And Languages

;; babel shizzle
(org-babel-do-load-languages
 'org-babel-load-languages
 '((python . t)
   (ditaa . t)
   (C . t)
   (R . t)
   (calc . t)
   (gnuplot . t)
   ;;(c++. t)
   (sh . t)
   ;;(dot. t)
   (java . t)
   (matlab . t)
   (org . t)
   (perl . t)
   ;;(shell . t)
   (sql . t)
   (asymptote . t)
))


;;Flyspell
(add-hook 'org-mode-hook 'flyspell-mode)
(add-hook 'org-mode-hook 'flyspell-buffer)

(setq org-latex-pdf-process (quote ("texi2dvi --pdf --clean --verbose --batch %f")))


;;Set the correct path to ditaa
;;We could do with setting up various directorys here
(setq org-ditaa-jar-path "~/emacsConf/ditaa/ditaa0_9.jar")

