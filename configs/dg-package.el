;; Dans Awesome Packages Stuff
1;95;0c1;95;0c
(provide 'dg-package)

;;Now we load emacs's version and sort this out
(require 'package)

(add-to-list 'package-archives
	     '("melpa" . "http://melpa.milkbox.net/packages/") t)
(add-to-list 'package-archives
	     '("marmalade" . "http://marmalade-repo.org/packages/") t)

(package-initialize)

(message "%s" "Checking package requirements")

;;And the Global package list
(add-to-list 'required-packages 'ox-reveal)


(dolist (p required-packages)
  (message "Checking for %s" p)
  (when (not (package-installed-p p))
    (message "--> %s Not Installed" p)
    (condition-case err
	;;Stuff that will always be executed
	(progn
          (package-install p)
	  )
      (error
       ;;Only evaluuated in case of error
       (message "--> Error Installing Package, attempting refresh")
       (package-refresh-contents)
       (package-install p)
       )
    )
  )
)


