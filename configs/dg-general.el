;; ------ GENERAL CONFIG FOR EMACS

(provide 'dg-general)

(setq inhibit-startup-screen t)
(setq transient-mark-mode t)

(fset 'yes-or-no-p 'y-or-n-p)

;;Line and Columns
(setq line-number-mode t)
(setq column-number-mode t)
;;(toggle-scroll-bar t)

;; Line wrap at 80
(set-default 'fill-column 80)


;; FLyspell Shizzle
(autoload 'flyspell-mode "flyspell" "On-the-fly spelling checker." t)
(autoload 'flyspell-delay-command "flyspell" "Delay on command." t)
(autoload 'tex-mode-flyspell-verify "flyspell" "" t) 
(add-hook 'LaTeX-mode-hook (lambda() (flyspell-mode 1)))
