;;General config for ORG mode etc

(provide 'dotconfig)

;;Get Ditaa
(setq org-ditaa-jar-path "emacsConf/ditaa0_9.jar")

(org-babel-do-load-languages
 'org-babel-load-languages
 '((python . t)
   (ditaa . t)
   (C . t)
   (R . t)
   (calc . t)
   (gnuplot . t)
   (cpp . t)
   (sh . t)
   (dot. t)
   (java . t)
   (matlab . t)
   (org . t)
   (perl . t)
   (shell . t)
   (sql . t)
   (asymptote . t)
))


;; FLyspell Shizzle
(autoload 'flyspell-mode "flyspell" "On-the-fly spelling checker." t)
(autoload 'flyspell-delay-command "flyspell" "Delay on command." t)
(autoload 'tex-mode-flyspell-verify "flyspell" "" t) 
(add-hook 'LaTeX-mode-hook (lambda() (flyspell-mode 1)))

