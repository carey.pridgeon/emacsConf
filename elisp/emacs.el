;;  ----------- COPY THIS IF YOU WANT PACKAGE MAGIC -------------
;;Load some global setup stuff

(add-to-list 'load-path "emacsConf")
;;Initialise global variables
(require 'dg-init)

;;Setup Package Library
(package-initialize)


;;Load The rest of the cofigurrations
(require 'dg-package)
(require 'dg-general)
(require 'dg-org)
(require 'ox-reveal)
(require 'ox-bibtex)
;; ------------ YOUR CONFIG CAN GO HERE ---------------
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["#242424" "#e5786d" "#95e454" "#cae682" "#8ac6f2" "#333366" "#ccaa8f" "#f6f3e8"])
 '(custom-enabled-themes (quote (light-blue)))
 '(custom-safe-themes
   (quote
    ("8db4b03b9ae654d4a57804286eb3e332725c84d7cdab38463cb6b97d5762ad26" "a25c42c5e2a6a7a3b0331cad124c83406a71bc7e099b60c31dc28a1ff84e8c04" default)))
 '(package-selected-packages (quote (nyan-mode ox-reveal color-theme-solarized))))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(add-to-list 'load-path "/usr/local/share/asymptote")
(autoload 'asy-mode "asy-mode.el" "Asymptote major mode." t)
(autoload 'lasy-mode "asy-mode.el" "hybrid Asymptote/Latex major mode." t)
(autoload 'asy-insinuate-latex "asy-mode.el" "Asymptote insinuate LaTeX." t)
(add-to-list 'auto-mode-alist '("\\.asy$" . asy-mode))


(setq org-src-fontify-natively t)
(load-file "~/Dropbox/emacs/elisp/sensible-defaults.el")
(sensible-defaults/use-all-settings)
(sensible-defaults/use-all-keybindings)
(add-to-list 'load-path "~/elisp")

(require 'org-bullets)
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
;; Add minted to the defaults packages to include when exporting.
(add-to-list 'org-latex-packages-alist '("" "minted"))
;; Tell the latex export to use the minted package for source
;; code coloration.
(setq org-latex-listings 'minted)
(setq org-reveal-root "file:///~/ab0475/Dropbox/emacs/reveal\
.js/js/reveal.js")
(setq org-latex-pdf-process (quote ("texi2dvi --pdf --clean --verbose --shell-escape --batch %f")))
(setq org-startup-indented t)
(menu-bar-mode -1)
